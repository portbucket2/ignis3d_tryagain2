﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class burnballController : MonoBehaviour
{
    public GameObject parentSet;

    public Vector3 targetPos;
    public GameObject target;
    public int targetID;
    public int tappedID;

    public bool reverse;
    public int reverseFac;

    public bool stop;
    public Vector3 stopPos;
    // Start is called before the first frame update
    private void Awake()
    {
        if (reverse)
        {
            reverseFac = -1;
        }
        else
        {
            reverseFac = 1;
        }
    }

    void Start()
    {
        //targetPos = target.transform.position;
        //if (reverse)
        //{
        //    reverseFac = -1;
        //}
        //else
        //{
        //    reverseFac = 1;
        //}
        //if (reverse)
        //{
        //    targetID = tappedID - 1;
        //}
        //else
        //{
        //    targetID = tappedID + 1;
        //}

        //targetID = tappedID;

    }

    // Update is called once per frame
    void Update()
    {
        DetermineTarget();
        
        if (!stop)
        {
            Advance();
        }
        else
        {
            this.transform.position = stopPos;
        }
        
    }

    void Advance()
    {
        Vector3 dir = targetPos - this.transform.position;
        this.transform.Translate(dir.normalized * Time.deltaTime*8);
    }

    void DetermineTarget()
    {
        //target = parentSet
        target = parentSet.GetComponent<burningManagement>().burnBlocks[targetID];
        targetPos = target.transform.position;
        if (Vector3.Distance(targetPos,this.transform.position)< 0.4f)
        {
            //targetPos += new Vector3(0, 0, 5);
            if (!reverse)
            {
                if (targetID < parentSet.GetComponent<burningManagement>().burnBlocks.Length - 1)
                {
                    targetID += 1 ;
                }
                else if (targetID >= parentSet.GetComponent<burningManagement>().burnBlocks.Length - 1)
                {
                    targetID = 0;
                }
            }
            else if (reverse)
            {
                if (targetID <= parentSet.GetComponent<burningManagement>().burnBlocks.Length - 1 && targetID > 0)
                {
                    targetID -= 1 ;
                }
                else if (targetID == 0)
                {
                    targetID = parentSet.GetComponent<burningManagement>().burnBlocks.Length - 1;
                }
            }

            
            
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "burnBlock")
        {
            //other.gameObject.SetActive(false);
            other.GetComponent<burnBlock>().burnIt = true;
        }
        if(other.tag == "endOfBurn")
        {
            other.GetComponent<endOfBurn>().atEnd = true;
            stopPos = other.transform.position;
            stop = true;
            //this.gameObject.SetActive(false);
        }
    }
}
