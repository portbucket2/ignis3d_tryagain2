﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class endSpereRaycast : MonoBehaviour
{

    public bool isFixed;
    public float scale;
    public float maxScale;

    public bool striked;
    public bool scaleDown;

    public GameObject burstParticle;
    
    // Start is called before the first frame update
    void Start()
    {
        scale = 1;
        //burstParticle.SetActive(false);
        burstParticle.SetActive(true);
        Destroy(this.gameObject,2f);
    }
    //
    //// Update is called once per frame
    void Update()
    {
        
        if(scale > (maxScale-1))
        {
            scaleDown = true;
            //this.gameObject.SetActive(false);
        }
        if (scaleDown)
        {
            scale = Mathf.Lerp(scale, 0, Time.deltaTime * 4f);
        }
        else
        {
            scale = Mathf.Lerp(scale, maxScale, Time.deltaTime * 4f);
        }

        //if (isFixed)
        //{
        //    burstParticle.SetActive(true);
        //}
         
        //else
        //{
        //    this.gameObject.SetActive(false);
        //}
        this.transform.localScale = new Vector3(scale, scale, scale);
    }

    private void OnTriggerEnter(Collider other)
    {
        //Destroy(other.gameObject);
        if (other.tag == "endSphere" && Vector3.Distance(this.transform.position,other.transform.position )< 1.5f && !isFixed)
        {
            //this.gameObject.SetActive(false);
            //Destroy(other.gameObject);
            //other.gameObject.SetActive(false);
            this.transform.position = other.transform.position;
            


            other.gameObject.GetComponent<endSpereRaycast>().isFixed = true;
            //if(scale < 2f)
            //{
            //    this.gameObject.SetActive(false);
            //}
            
            //this.transform.position = other.transform.position;
        }
        if(other.tag == "burnBlock" && striked == false  )
        {
            other.gameObject.GetComponent<burnCube>().active = true;
            
            striked = true;
            scaleDown = true;
        }
    }




}
