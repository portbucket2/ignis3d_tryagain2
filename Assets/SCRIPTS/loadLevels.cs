﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class loadLevels : MonoBehaviour
{
    public GameObject[] levelsPrefab;
    public GameObject currentLevel;
    public Vector3[] cameraPoses;
    public Vector3 currentCamPos;
    public GameObject cam;
    public GameObject panelOutOfLevels;
    // Start is called before the first frame update
    void Start()
    {
        //LoadLevel();
        //cam = Camera.main;
        currentLevel = Instantiate(currentLevel);
        currentCamPos = cam.transform.position;
        panelOutOfLevels.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        UpdateCameraPos();
    }

    public void LoadLevel()
    {
        Destroy(currentLevel.gameObject);
        gameManagement.cubesInScene = 0;
        currentLevel = Instantiate(levelsPrefab[gameManagement.level-1 ]);
        PlayerPrefs.SetInt("level", gameManagement.level);
        //currentLevel = 
    }

    public void UpdateNextLevel()
    {
        if(gameManagement.level < gameManagement.maxLevel)
        {
            gameManagement.level += 1;
        }
        else
        {
            panelOutOfLevels.SetActive(true);
        }

        //gameManagement.level += 1;
        uIManager.doUiUpdate = true;
        PlayerPrefs.SetInt("level",gameManagement. level);
    }
    

    public void UpdateCameraPos()
    {
        currentCamPos = Vector3.Lerp(currentCamPos, cameraPoses[gameManagement.level - 1], Time.deltaTime * 5f);
        cam.transform.position = currentCamPos;
    }

    public void ShutPanelOutOfLevels()
    {
        panelOutOfLevels.SetActive(false);
    }
}
