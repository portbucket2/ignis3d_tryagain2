﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class midBlockmanager : MonoBehaviour
{
    public GameObject[] midBlocks;
    //public float[] rightAngles; 
    // Start is called before the first frame update
    void Start()
    {
        int m = Random.Range(0, midBlocks.Length );
        for (int i = 0; i < midBlocks.Length; i++)
        {
            if(i == m)
            {
                midBlocks[i].SetActive(true);
            }
            else
            {
                midBlocks[i].SetActive(false);
            }
        }

        int a = Random.Range(0, 4);
        if(a == 0)
        {
            midBlocks[m].transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
        }
        else if(a == 1)
        {
            midBlocks[m].transform.localRotation = Quaternion.Euler(new Vector3(0, 90, 0));
        }
        else if (a == 2)
        {
            midBlocks[m].transform.localRotation = Quaternion.Euler(new Vector3(0, 180, 0));
        }
        else if (a == 3)
        {
            midBlocks[m].transform.localRotation = Quaternion.Euler(new Vector3(0, -90, 0));
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
