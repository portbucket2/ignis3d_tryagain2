﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelGenerator : MonoBehaviour
{
    public Transform[] corners;
    public GameObject buildMesh;
    public float[] distances;
    //public Vector3 buildingPos;

    //public bool[] front;
    //public bool[] right;
    //public bool[] back;
    //public bool[] left;
    //
    //public GameObject[] frontTurns;
    //public GameObject[] rightTurns;
    //public GameObject[] backTurns ;
    //public GameObject[] leftTurns ;

    //public GameObject[] frontRoad;
    //public GameObject[] rightRoad;
    //public GameObject[] backRoad;
    //public GameObject[] leftRoad;

    public GameObject[] cornerTags;
    public Transform[] roadCorner;

    public GameObject[] buildingPositioner;
    //public LayerMask roadLayer;
    // Start is called before the first frame update
    void Start()
    {
        MakeBlock();
        //RaycastingAround();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void MakeBlock()
    {
        distances[0] = Vector3.Distance(corners[0].position, corners[1].position);
        //buildingPos = corners[0].position;

        for (int i = 0; i < Mathf.Abs(distances[0]) - 0.5f ; i++)
        {
            GameObject go =  Instantiate(buildMesh, buildingPositioner[0].transform.position, buildingPositioner[0].transform.rotation);
            go.transform.SetParent(transform);
            buildingPositioner[0].transform.localPosition += Vector3.right;
            //buildingPos.x += 1;
        }

        distances[1] = Vector3.Distance(corners[1].position, corners[2].position);
        //buildingPos = corners[1].position;

        for (int i = 0; i < Mathf.Abs(distances[1]) - 0.5f; i++)
        {
            GameObject go = Instantiate(buildMesh, buildingPositioner[1].transform.position, buildingPositioner[0].transform.rotation);
            go.transform.SetParent(transform);
            buildingPositioner[1].transform.localPosition += Vector3.right;
            //buildingPos.z += 1;
        }
        distances[2] = Vector3.Distance(corners[2].position, corners[3].position);
        //buildingPos = corners[2].position;

        for (int i = 0; i < Mathf.Abs(distances[2]) - 0.5f; i++)
        {
            GameObject go = Instantiate(buildMesh, buildingPositioner[2].transform.position, buildingPositioner[0].transform.rotation);
            go.transform.SetParent(transform);
            buildingPositioner[2].transform.localPosition += Vector3.right;
            //buildingPos.x -= 1;
        }
        distances[3] = Vector3.Distance(corners[3].position, corners[0].position);
        //buildingPos = corners[3].position;

        for (int i = 0; i < Mathf.Abs(distances[3]) - 0.5f; i++)
        {
            GameObject go = Instantiate(buildMesh, buildingPositioner[3].transform.position, buildingPositioner[0].transform.rotation);
            go.transform.SetParent(transform);
            buildingPositioner[3].transform.localPosition += Vector3.right;
            //buildingPos.z -= 1;
        }

        for (int i = 0; i < 4; i++)
        {
            cornerTags[i].SetActive(false);
        }
    }

    //void RaycastingAround()
    //{
    //    RaycastHit hit;
    //
    //    for (int i = 0; i < 4; i++)
    //    {
    //        if (Physics.Raycast(roadCorner[i].transform.position, transform.forward, 10f, roadLayer))
    //        {
    //            front[i] = true;
    //            frontTurns[i].SetActive(true);
    //        }
    //        else
    //        {
    //            frontRoad[i].SetActive(true);
    //        }
    //        if (Physics.Raycast(roadCorner[i].transform.position, transform.right, 10f, roadLayer))
    //        {
    //            right[i] = true;
    //            rightTurns[i].SetActive(true);
    //        }
    //        else
    //        {
    //            rightRoad[i].SetActive(true);
    //        }
    //        if (Physics.Raycast(roadCorner[i].transform.position, -transform.forward, 10f, roadLayer))
    //        {
    //            back[i] = true;
    //            backTurns[i].SetActive(true);
    //        }
    //        else
    //        {
    //            backRoad[i].SetActive(true);
    //        }
    //        if (Physics.Raycast(roadCorner[i].transform.position, -transform.right, 10f, roadLayer))
    //        {
    //            left[i] = true;
    //            leftTurns[i].SetActive(true);
    //        }
    //        else
    //        {
    //            leftRoad[i].SetActive(true);
    //        }
    //    }
    //    
    //}
}
