﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class roadCornerManager : MonoBehaviour
{
    public LayerMask roadLayer;

    public bool front;
    public bool right;
    public bool back;
    public bool left;

    public GameObject[] roadTurns;
    public GameObject[] roadStraights;
    public float raycastRange;

    public GameObject roadStraight;
    public Transform rightTarget;
    public GameObject roadPositioner;

    public float RightTargetdistance;
    // Start is called before the first frame update
    void Start()
    {
        ConnectRoad();
        Invoke("RaycastingAround", 0.1f);
        //RaycastingAround();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void RaycastingAround()
    {
        RaycastHit hit;
        if (Physics.Raycast(this.transform.position, transform.forward, raycastRange, roadLayer))
        {
            front = true;
            roadTurns[0].SetActive(true);
        }
        else
        {
            roadStraights[0].SetActive(true);
        }
        if (Physics.Raycast(this.transform.position, transform.right,out hit, raycastRange, roadLayer))
        {
            right = true;
            //rightTarget = hit.transform;
            //ConnectRoad();
            roadTurns[1].SetActive(true);
        }
        else
        {
            roadStraights[1].SetActive(true);
        }
        if (Physics.Raycast(this.transform.position, -transform.forward, raycastRange, roadLayer))
        {
            back = true;
            roadTurns[2].SetActive(true);
        }
        else
        {
            roadStraights[2].SetActive(true);
        }
        if (Physics.Raycast(this.transform.position, -transform.right, raycastRange, roadLayer))
        {
            left = true;
            roadTurns[3].SetActive(true);
        }
        else
        {
            roadStraights[3].SetActive(true);
        }

        //for (int i = 0; i < 4; i++)
        //{
        //    
        //}

    }

    void ConnectRoad()
    {
        RightTargetdistance = Vector3.Distance(this.transform.position, rightTarget.position);
        Vector3 roadStraightPos = this.transform.localPosition + Vector3.right;
        //roadPositioner = 
        for (int i = 0; i < RightTargetdistance - 1.1f; i++)
        {
            GameObject go =  Instantiate(roadStraight, roadPositioner.transform.position, Quaternion.identity);
            go.transform.SetParent(transform);
            go.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
            roadPositioner.transform.localPosition += Vector3.right;
        }
    }
}
